import BaseHTTPServer

class RequestHandler (BaseHTTPServer.BaseHTTPRequestHandler):
    """ Handle HTTP requests by returning a fixed page """
    Page= """\
    <html>
    <body>
    <p>Hello world, web!sadasdasda</p>
    </body>
    </html> """

    def do_GET(self)
        self.send_content(200,self.Page)

    def send_content(self,status_code,content):
        self.send_response(status_code)
        self.send_header("Content-Type","text/html")
        self.send_header("Content-Lenght",str(len(self.Page)))
        self.end_headers()
        self.wfile.write(self.Page)

if  __name__ == "__main__":
    print "Starting web server...."
    server_address=('',8080)

    server = BaseHTTPServer.HTTPServer(
        server_address, RequestHandler)
    server.serve_forever()
